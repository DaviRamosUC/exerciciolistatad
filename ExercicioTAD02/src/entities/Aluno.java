package entities;

import java.util.List;

import services.AlunoService;

public class Aluno implements AlunoService {

	private String nome;
	private int semestre;

	public Aluno() {

	}

	public Aluno(String nome, int semestre) {
		this.nome = nome;
		this.semestre = semestre;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getSemestre() {
		return semestre;
	}

	public void setSemestre(int semestre) {
		this.semestre = semestre;
	}

	@Override
	public void armazenaAluno(List<Aluno> alunos, Aluno aluno) {
		alunos.add(aluno);

	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Aluno: "+ nome);
		sb.append(", semestre: "+semestre);
		return sb.toString();
	}
	
	

}
