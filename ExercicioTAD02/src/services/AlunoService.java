package services;

import java.util.List;

import entities.Aluno;

public interface AlunoService {
	
	public void armazenaAluno(List<Aluno> alunos, Aluno aluno);
}
