package application;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import entities.Aluno;

public class Program {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		List<Aluno> alunos = new ArrayList<>();
		Aluno aluno = new Aluno();
		
		char saida = 's';
		while (saida=='s') {
		System.out.print("Informe o nome do aluno: ");
		String nome = sc.nextLine();
		System.out.print("Informe o semestre do aluno: ");
		int semestre = sc.nextInt();
		sc.nextLine();
		aluno = new Aluno(nome, semestre);
		
		aluno.armazenaAluno(alunos, aluno);
		
		System.out.print("Deseja armazenar mais um aluno? s/n: ");
		saida = sc.nextLine().charAt(0);
		}
		System.out.println("\nALUNOS CADASTRADOS: ");
		for (Aluno a : alunos) {
			System.out.println(a.toString());
		}
		
		
		sc.close();
	}

}
