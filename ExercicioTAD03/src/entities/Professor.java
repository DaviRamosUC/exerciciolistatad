package entities;

import java.util.List;

import service.ProfessorService;

public class Professor implements ProfessorService {

	private String nome;
	private int codigoDisciplina;

	public Professor() {

	}

	public Professor(String nome, int codigoDisciplina) {
		this.nome = nome;
		this.codigoDisciplina = codigoDisciplina;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getCodigoDisciplina() {
		return codigoDisciplina;
	}

	public void setCodigoDisciplina(int codigoDisciplina) {
		this.codigoDisciplina = codigoDisciplina;
	}

	@Override
	public void armazenaProfessor(List<Professor> professores, Professor professor) {

		professores.add(professor);

	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(nome);
		sb.append(", codigo da disciplina: " +codigoDisciplina);
		return sb.toString();
	}
	
	

}
