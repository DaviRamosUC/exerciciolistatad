package application;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import entities.Professor;

public class Program {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		List<Professor> professores = new ArrayList<>();
		
		char saida = 's';
		while (saida == 's') {
			System.out.print("Informe o nome do professor: ");
			String nome = sc.nextLine();
			System.out.print("Informe o c�digo da disciplina: ");
			int codigoDisciplina = sc.nextInt();
			
			professores.add(new Professor(nome, codigoDisciplina));
			
			System.out.print("Deseja armazenar mais um professor? s/n: ");
			sc.nextLine();
			saida = sc.nextLine().charAt(0);
		}
		
		System.out.println("\nPROFESSORES CADASTRADOS: ");
		for (Professor p : professores) {
			System.out.println(p.toString());
		}

		sc.close();

	}

}
