package service;

import java.util.List;

import entities.Professor;

public interface ProfessorService {
	
	public void armazenaProfessor(List<Professor> professores, Professor professor);
}
