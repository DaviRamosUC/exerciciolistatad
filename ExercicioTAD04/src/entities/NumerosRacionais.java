package entities;

import java.util.List;

import service.NumerosRacionaisService;

public class NumerosRacionais implements NumerosRacionaisService {
		
	public NumerosRacionais() {
		
	}
	
	@Override
	public String criarRacional(int n1, int n2, List<Double> racional) {
		racional.add((double) (n1/n2));
		return n1+"/"+n2;
	}

	@Override
	public boolean testarIgualdade(List<Double> racional,int index1, int index2) {
		return racional.get(index1).equals(racional.get(index2));
	}

	@Override
	public double somarRacionais(List<Double> racional,int index1, int index2) {
		return racional.get(index1)+racional.get(index2);
	}

	@Override
	public double multiplicarRacionais(List<Double> racional,int index1, int index2) {
		return racional.get(index1)*racional.get(index2);
	}
	
	
	
}
