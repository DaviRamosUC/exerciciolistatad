package application;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import entities.NumerosRacionais;

public class Program {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		NumerosRacionais nr = new NumerosRacionais();
		List<Double> racionais = new ArrayList<>();

		char saida = 's';
		while (saida == 's') {
			System.out.print("Informe o primeiro termo inteiro: ");
			int n1 = sc.nextInt();
			System.out.print("Informe o segundo termo inteiro: ");
			int n2 = sc.nextInt();
			System.out.println(nr.criarRacional(n1, n2, racionais));
			System.out.print("Deseja armazenar mais um n�mero racional? s/n: ");
			sc.nextLine();
			saida = sc.nextLine().charAt(0);
		}
		
		int resposta =0, index1,index2;
		while (resposta != 4) {
			System.out.println("Informe o que voc� deseja fazer: (1)Testar igualdade, "
					+ "(2)Somar Racionais, (3)Multiplicar Racionais, (4)Terminar Execu��o");
			resposta=sc.nextInt();
			switch (resposta) {
			case 1: {
				System.out.println("Qual dos n�meros voc� quer testar? de 0 at� "+(racionais.size()-1));
				index1=sc.nextInt();
				System.out.println("Qual o segundo n�mero? de 0 at� "+(racionais.size()-1));
				index2=sc.nextInt();
				System.out.println((nr.testarIgualdade(racionais, index1, index2)) ? "Iguais": "Diferentes");
				break;
			}
			case 2: {
				System.out.println("Qual dos n�meros voc� quer testar? de 0 at� "+(racionais.size()-1));
				index1=sc.nextInt();
				System.out.println("Qual o segundo n�mero? de 0 at� "+(racionais.size()-1));
				index2=sc.nextInt();
				System.out.println("A soma dos dois termos � igual: " + nr.somarRacionais(racionais, index1, index2));
				break;
			}
			case 3: {
				System.out.println("Qual dos n�meros voc� quer testar? de 0 at� "+(racionais.size()-1));
				index1=sc.nextInt();
				System.out.println("Qual o segundo n�mero? de 0 at� "+(racionais.size()-1));
				index2=sc.nextInt();
				System.out.println("O produto dos dois termos � igual: " + nr.multiplicarRacionais(racionais, index1, index2));
				break;
			}
			case 4: {
				System.out.println("Execu��o Terminada");
				break;
			}
			default:
				System.out.println("Op��o n�o v�lida");
			}
		}

		sc.close();

	}

}
