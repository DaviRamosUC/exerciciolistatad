package service;

import java.util.List;

public interface NumerosRacionaisService {

		public boolean testarIgualdade(List<Double> racional,int index1,int index2);
		public String criarRacional(int n1, int n2, List<Double> recional);
		public double somarRacionais(List<Double> racional,int index1,int index2);
		public double multiplicarRacionais(List<Double> racional,int index1,int index2);
		
}
