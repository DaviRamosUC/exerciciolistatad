package entities;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import services.LivroService;

public class Livro implements LivroService{
	
	private String titulo;
	private String editora;
	private Date anoDePublicacao;
	
	public Livro() {
		
	}

	public Livro(String titulo, String editora, Date anoDePublicacao) {
		this.titulo = titulo;
		this.editora = editora;
		this.anoDePublicacao = anoDePublicacao;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getEditora() {
		return editora;
	}

	public void setEditora(String editora) {
		this.editora = editora;
	}

	public Date getAnoDePublicacao() {
		return anoDePublicacao;
	}

	public void setAnoDePublicacao(Date anoDePublicacao) {
		this.anoDePublicacao = anoDePublicacao;
	}

	@Override
	public void armazenaLivro(List<Livro> livros, Livro livro) {
		
		livros.add(livro);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		SimpleDateFormat sdf  = new SimpleDateFormat("yyyy");
		sb.append("\nO livro \""+titulo+"\"");
		sb.append(" pertence a editora "+editora);
		sb.append(" e foi lan�ado em "+sdf.format(anoDePublicacao));
		return sb.toString();
	}
	
	

	
	
}
