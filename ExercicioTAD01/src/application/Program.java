package application;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import entities.Livro;

public class Program {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
		
		List<Livro> livros = new ArrayList<>();
		Livro livro = new Livro();
		
		System.out.println("CADASTRO LIVRO");
		System.out.print("Infome o titulo do livro: ");
		String titulo = sc.nextLine();
		System.out.print("Infome a editora do livro: ");
		String editora = sc.nextLine();
		System.out.print("Infome o ano de publicação do livro: ");
		
		try {
			Date anoPublicacao = sdf.parse(sc.nextLine());
			livro = new Livro(titulo, editora, anoPublicacao);
		} catch (ParseException e) {
			System.out.println("Error: "+e.getMessage());
		}
		
		livro.armazenaLivro(livros, livro);
		
		for (Livro l : livros) {
			System.out.println(l.toString());
		}
		
		
		sc.close();

	}

}
