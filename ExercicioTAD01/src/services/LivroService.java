package services;

import java.util.List;

import entities.Livro;

public interface LivroService {
	
	public void armazenaLivro(List<Livro> livros, Livro livro);
}
