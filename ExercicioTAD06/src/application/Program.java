package application;

import java.util.Scanner;

import entities.Agenda;
import entities.Contato;

public class Program {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.print("Informe um nome para a agenda: ");
		String nomeAgenda = sc.nextLine();
		Agenda agenda = new Agenda(nomeAgenda);

		int op = 0;
		while (op != 5) {
			System.out.println("\nDeseja fazer qual opera��o? \n(1) Adicionar Contato "
					+ "\n(2) Remover Contato \n(3) Excluir Agenda \n(4) Atualizar Agenda \n(5) Finalizar Execu��o");
			op = sc.nextInt();
			sc.nextLine();

			switch (op) {
			case 1: {
				System.out.print("Informe o nome do contato a ser cadastrado: ");
				String nomeContato = sc.nextLine();
				System.out.print("Informe o n�mero do contato a ser cadastrado: ");
				String numeroContato = sc.nextLine();
				agenda.adicionar(new Contato(nomeContato, numeroContato));
				break;
			}
			case 2: {
				System.out.print("Informe o nome do contato a ser removido: ");
				String nomeContato = sc.nextLine();
				agenda.remover(nomeContato);
				
				break;
			}
			case 3: {
				agenda.excluir();
				System.out.println("Agenda vazia");
				break;
			}
			case 4: {
				System.out.println("\nCONTATOS NA AGENDA: ");
				agenda.atualizar();
				break;
			}
			case 5: {
				System.out.println("Execu��o terminada");
				break;
			}
			default:
				System.out.println("Op��o n�o v�lida, tente novamente!");
			}
		}

		sc.close();
	}

}
