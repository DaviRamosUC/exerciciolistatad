package service;

import entities.Contato;

public interface AgendaServices {
	
	public void adicionar(Contato contato);
	public void remover(String nomeContato);
	public void excluir();
	public void atualizar();
	
}
