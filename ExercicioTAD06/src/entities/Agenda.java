package entities;

import java.util.HashMap;
import java.util.Map;

import service.AgendaServices;

public class Agenda implements AgendaServices {

	private String nomeAgenda;
	private Map<String, String> contatos = new HashMap<>();

	public Agenda() {

	}

	public Agenda(String nomeAgenda) {
		this.nomeAgenda = nomeAgenda;
	}

	public String getNomeAgenda() {
		return nomeAgenda;
	}

	public void setNomeAgenda(String nomeAgenda) {
		this.nomeAgenda = nomeAgenda;
	}

	@Override
	public void adicionar(Contato contato) {
		contatos.put(contato.getNomeContato(), contato.getNumeroContato());
	}

	@Override
	public void remover(String nomeContato) {
		contatos.remove(nomeContato);

	}

	@Override
	public void excluir() {
		contatos.clear();

	}

	@Override
	public void atualizar() {
		for (String key : contatos.keySet()) {
			String numero = contatos.get(key);
			System.out.println(key + ": "+ numero);
		}
		
	}

}
