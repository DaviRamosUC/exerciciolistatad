package services;

public interface ContaBancariaService {
	
	public void depositar(double valor);
	public void sacar(double valor);
	public void atualizar(String nome, int numeroConta);
	public double visualizarSaldo();
	
}
