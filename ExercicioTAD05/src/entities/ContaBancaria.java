package entities;

import services.ContaBancariaService;

public class ContaBancaria implements ContaBancariaService {

	private double saldo;
	private String nome;
	private int numeroConta;

	public ContaBancaria() {

	}

	public ContaBancaria(String nome, int numeroConta) {
		this.nome = nome;
		this.numeroConta = numeroConta;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getNumeroConta() {
		return numeroConta;
	}

	public void setNumeroConta(int numeroConta) {
		this.numeroConta = numeroConta;
	}

	@Override
	public void depositar(double valor) {
		saldo+=valor;
		
	}

	@Override
	public void sacar(double valor) {
		saldo-=valor;
		
	}

	@Override
	public void atualizar(String nome, int numeroConta) {
		this.nome=nome;
		this.numeroConta=numeroConta;
		
	}

	@Override
	public double visualizarSaldo() {
		return saldo;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("DADOS DA CONTA: \n");
		sb.append("Nome: "+ nome);
		sb.append("\nN�mero da conta: "+ numeroConta);
		sb.append("\nSaldo: "+ saldo);
		
		return sb.toString();
	}
	
	
	
	
	
}
