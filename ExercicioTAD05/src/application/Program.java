package application;

import java.util.Scanner;

import entities.ContaBancaria;

public class Program {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("CADASTRO DE NOVA CONTA BANC�RIA");
		System.out.print("Informe seu nome: ");
		String nome = sc.nextLine();
		System.out.println("Informe o n�mero da conta: ");
		int numeroConta = sc.nextInt();
		
		
		ContaBancaria c = new ContaBancaria(nome, numeroConta);
		
		int op=0;
		while (op != 5) {
			System.out.println("Deseja fazer qual opera��o? \n(1) Visualizar Saldo "
					+ "\n(2) Depositar \n(3) Sacar \n(4) Atualizar Conta \n(5) Finalizar Execu��o");
			op=sc.nextInt();
			switch (op) {
			case 1: {
				System.out.println("R$ "+c.visualizarSaldo());
				break;
			}
			case 2: {
				System.out.print("Informe o valor a ser depositado: ");
				double valor = sc.nextDouble();
				c.depositar(valor);
				System.out.println("R$ "+c.visualizarSaldo());
				break;
			}
			case 3: {
				System.out.print("Informe o valor a ser sacado: ");
				double valor = sc.nextDouble();
				c.sacar(valor);
				System.out.println("R$ "+c.visualizarSaldo());
				break;
			}
			case 4: {
				sc.nextLine();
				System.out.print("Informe o novo nome a ser cadastrado: ");
				nome = sc.nextLine();
				System.out.println("Informe o novo n�mero da conta: ");
				numeroConta = sc.nextInt();
				c.atualizar(nome, numeroConta);
				System.out.println(c.toString());
				break;
			}
			case 5: {
				System.out.println("Execu��o Terminada");
				break;
			}
			default:
				System.out.println("Op��o n�o v�lida, tente novamente!");
			}
		}
		
		sc.close();
	}

}
